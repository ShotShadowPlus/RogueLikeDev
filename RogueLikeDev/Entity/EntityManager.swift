import SpriteKit

class EntityManager {
    static var shared = EntityManager()

    private var mPlayer: Entity!
    private var mNPCs = [Entity]()

    private var npcsToBeAdded = [Entity]()

    private init () {
        let fighterComponent = Fighter(hp: 30, defense: 2, power: 5)

        mPlayer = Entity(position: MapPosition(), name: "player", fighterComponent: fighterComponent, aiComponent: nil)
        mPlayer.mVisible = true
        mPlayer.mBlocks = true
        npcsToBeAdded.append(mPlayer)
    }

    func getPlayer() -> Entity {
        return mPlayer
    }

    func getNPCs() -> [Entity] {
        return mNPCs
    }

    func addNPC(_ npc: Entity) {
        self.mNPCs.append(npc)
        self.npcsToBeAdded.append(npc)
    }

    func getEntities(at position: MapPosition) ->[Entity] {
        var result = [Entity]()

        if mPlayer != nil {
            if position.x == mPlayer.mPosition.x && position.y == mPlayer.mPosition.y {
                result.append(mPlayer)
            }
        }

        for entity in mNPCs {
            if position.x == entity.mPosition.x && position.y == entity.mPosition.y {
                result.append(entity)
            }
        }

        return result
    }

    func getNPCsToBeAttached() -> [Entity] {
        return npcsToBeAdded
    }

    func clearNPCsToBeAttached() {
        npcsToBeAdded.removeAll()
    }
}
