import SpriteKit

enum EntityRenderOrder{
    case CORPSE
    case ITEM
    case ACTOR

    func getValue() -> CGFloat {
        switch self {
        case .CORPSE:
            return 0
        case .ITEM:
            return 1
        case .ACTOR:
            return 2
        }
    }
}

class Entity {
    var mName = "EntityName"

    var mSpriteNode = SKSpriteNode(color: .red, size: TILE_SIZE)
    var mVisionRadius = UInt32(10)
    var mBlocks = false
    var mEntityRenderOrder = EntityRenderOrder.ACTOR

    var mFighterComponent: FighterComponent?
    private var mAIComponent: AIComponent?

    private var mpDead = false
    private var mpVisible = false

    var mDead: Bool {
        get { return mpDead }
        set { mpDead = newValue; calculateTexture() }
    }

    var mVisible: Bool {
        get { return mpVisible }
        set { mpVisible = newValue; calculateTexture() }
    }

    init (position: MapPosition, name: String, fighterComponent: FighterComponent?, aiComponent: AIComponent?) {
        self.mSpriteNode.anchorPoint = CGPoint(x: 0, y: 0)
        self.mPosition = position
        self.mName = name
        self.mVisible = false
        self.mDead = false

        self.mFighterComponent = fighterComponent
        if self.mFighterComponent != nil {
            self.mFighterComponent!.mEntity = self
        }

        self.mAIComponent = aiComponent
        if self.mAIComponent != nil {
            self.mAIComponent!.mEntity = self
        }
    }

    var mPosition: MapPosition {
        get { return MapPosition(position: self.mSpriteNode.position) }
        set { self.mSpriteNode.position = newValue.getCGPoint() }
    }

    func move(_ distance: CGVector) {
        let newPosition = mPosition
        newPosition.x += distance.dx
        newPosition.y += distance.dy
        self.mPosition = newPosition
    }

    func move(to position: MapPosition, gameMap: GameMap) {
        var movement = mPosition.getVector(to: position).normalize()
        movement.dx.round()
        movement.dy.round()

        let newPosition = mPosition.add(vector: movement)

        if !gameMap.isBlocked(position: newPosition)
            && EntityManager.shared.getEntities(at: newPosition).isEmpty {
            self.move(movement)
        }
    }

    private func calculateTexture() {
        if mDead {
            mEntityRenderOrder = .CORPSE
        } else {
            mEntityRenderOrder = .ACTOR
        }

        self.mSpriteNode.color = TextureManager.shared.getColor(objectName: self.mName, isDead: mDead)

        if self.mVisible {
            mSpriteNode.isHidden = false
        } else {
            mSpriteNode.isHidden = true
        }

        mSpriteNode.zPosition = mEntityRenderOrder.getValue()
    }

    func takeTurn(gameMap: GameMap) -> [ActionResult] {
        var actionResults = [ActionResult]()
        if mAIComponent == nil {
            return actionResults
        }

        actionResults.append(contentsOf: mAIComponent!.takeTurn(gameMap: gameMap) )
        return actionResults
    }
}
