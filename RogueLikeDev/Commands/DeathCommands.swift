func killPlayer(_ player: Entity) {
    player.mDead = true
}

func killNPC(_ npc: Entity) {
    npc.mDead = true
    npc.mBlocks = false
}
