import SpriteKit

class GameMap {
    var mSize: CGSize
    var mTiles = [MapPosition: Tile]()
    var mRooms = [CGRect]()

    init (size: CGSize) {
        self.mSize = size
        self.mTiles = self.initializeTiles()
    }

    private func initializeTiles() -> [MapPosition: Tile] {
        var tiles = [MapPosition: Tile]()

        for x in 0...Int(mSize.width)-1 {
            for y in 0...Int(mSize.height)-1 {
                let position = MapPosition(x: x, y: y)
                tiles[position] = Tile(position: position, blockMovement: true, blockSight: true)
            }
        }

        return tiles
    }

    func makeMap(maxRoomCount: UInt32, roomMinSize: UInt32, roomMaxSize: UInt32, mapSize: CGSize, maxMonstersPerRoom: UInt32) {
        for _ in 0...maxRoomCount {
            // want to include the edges
            let roomSize = CGSize(width: CGFloat(arc4random_uniform(roomMaxSize - roomMinSize + 1) + roomMinSize),
                                  height: CGFloat(arc4random_uniform(roomMaxSize - roomMinSize + 1) + roomMinSize))

            let roomPosition = CGPoint(x: CGFloat(arc4random_uniform(UInt32(mapSize.width - roomSize.width - 1))),
                                       y: CGFloat(arc4random_uniform(UInt32(mapSize.height - roomSize.height - 1))) )

            let newRoom = CGRect(origin: roomPosition, size: roomSize)

            var newRoomIntersection = false
            for room in mRooms {
                if newRoom.intersects(room) {
                    newRoomIntersection = true
                }
            }

            if !newRoomIntersection {
                // create tunnels to previous room only
                if mRooms.count > 0 {
                    createTunnel(startPoint: mRooms.last!.getCenter(), endPoint: newRoom.getCenter())
                }

                createRoom(rect: newRoom)
                placeEntities(room: newRoom, maxMonstersPerRoom: maxMonstersPerRoom)
            }
        }
    }

    private func createRoom(rect: CGRect) {
        for x in Int(rect.minX + 1)...Int(rect.maxX) {
            for y in Int(rect.minY + 1)...Int(rect.maxY) {
                if let tile = self.mTiles[MapPosition(x: x, y: y)] {
                    tile.mBlockMovement = false
                    tile.mBlockSight = false
                }
            }
        }

        mRooms.append(rect)
    }

    private func createTunnel(startPoint: MapPosition, endPoint: MapPosition) {
        if startPoint.x == endPoint.x {
            for y in Int(min(startPoint.y, endPoint.y))...Int(max(startPoint.y, endPoint.y)) {
                if let tile = self.mTiles[MapPosition(x: Int(startPoint.x), y: y)] {
                    tile.mBlockMovement = false
                    tile.mBlockSight = false
                }
            }
        } else if startPoint.y == endPoint.y {
            for x in Int(min(startPoint.x, endPoint.x))...Int(max(startPoint.x, endPoint.x)) {
                if let tile = self.mTiles[MapPosition(x: x, y: Int(startPoint.y))] {
                    tile.mBlockMovement = false
                    tile.mBlockSight = false
                }
            }
        } else {
            if arc4random_uniform(2) == 0 {
                createTunnel(startPoint: startPoint, endPoint: MapPosition(x: startPoint.x, y: endPoint.y))
                createTunnel(startPoint: MapPosition(x: startPoint.x, y: endPoint.y), endPoint: endPoint)
            } else {
                createTunnel(startPoint: startPoint, endPoint: MapPosition(x: endPoint.x, y: startPoint.y))
                createTunnel(startPoint: MapPosition(x: endPoint.x, y: startPoint.y), endPoint: endPoint)
            }
        }
    }

    func placeEntities(room: CGRect, maxMonstersPerRoom: UInt32) {
        let numberOfMonsters = arc4random_uniform(maxMonstersPerRoom)

        for _ in 0...numberOfMonsters {
            //choose random location in the room
            let position = MapPosition(x: CGFloat(arc4random_uniform(UInt32(room.width))) + room.minX + 1,
                                   y: CGFloat(arc4random_uniform(UInt32(room.height))) + room.minY + 1)

            if !EntityManager.shared.getEntities(at: position).isEmpty {
                continue
            }

            if arc4random_uniform(100) < 80 {
                let fighterComponent = Fighter(hp: 10, defense: 0, power: 3)
                let aiComponent = BasicMonster()

                let monster = Entity(position: position, name: "orc", fighterComponent: fighterComponent, aiComponent: aiComponent)
                monster.mBlocks = true
                monster.mVisionRadius = 5
                EntityManager.shared.addNPC(monster)
            } else {
                let fighterComponent = Fighter(hp: 16, defense: 1, power: 4)
                let aiComponent = BasicMonster()

                let monster = Entity(position: position, name: "troll", fighterComponent: fighterComponent, aiComponent: aiComponent)
                monster.mBlocks = true
                monster.mVisionRadius = 5
                EntityManager.shared.addNPC(monster)
            }
        }
    }

    func calculatePlayerVision(player: Entity) {
        // reset everything to hidden
        for tile in mTiles {
            tile.value.mVisible = false
        }

        // calculate tiles that are visible to player and make them visible
        let commandCalculateVision = CommandCalculateVisibleTiles(gameMap: self, playerPosition: player.mPosition, visionRadius: player.mVisionRadius)
        commandCalculateVision.process()
        for tile in commandCalculateVision.mVisibleTiles {
            tile.mExplored = true
            tile.mVisible = true
        }
    }

    func isBlocked(position: MapPosition) -> Bool {
        if let tile = mTiles[position] {
            if !tile.mBlockMovement {
                return false
            }
        }
        return true
    }

    func canSeePlayer(entity: Entity) -> Bool {
        let player = EntityManager.shared.getPlayer()
        if entity.mPosition.distance(to: player.mPosition) <= CGFloat(entity.mVisionRadius) {
            return true
        }
        return false
    }
}
