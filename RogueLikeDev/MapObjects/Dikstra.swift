import SpriteKit

extension PathFinderManager {
    class Dikstra {
        let mNodes: [MapPosition: Node]

        var mDistance = [MapPosition: Int]()
        var mPrevious = [MapPosition: MapPosition]()

        init (nodes: [MapPosition: Node]) {
            self.mNodes = nodes
        }

        func getPath(start: MapPosition, endPoints: [MapPosition]) -> [MapPosition] {
            mDistance.removeAll()
            mPrevious.removeAll()

            mDistance[start] = 0
            mPrevious[start] = start

            let pQueue = PQueue()
            pQueue.addNode(point: start, priority: 0)

            while pQueue.size() != 0 && endPoints.filter({mPrevious[$0] != nil}).isEmpty {
                let nextNode = pQueue.getNextNode()

                for connection in mNodes[nextNode]!.getNonBlockedConnections() {
                    let tempDistanceToConnection = mDistance[nextNode]! + 1
                    if mDistance[connection.id] == nil || tempDistanceToConnection < mDistance[connection.id]! {
                        mDistance[connection.id] = tempDistanceToConnection
                        mPrevious[connection.id] = nextNode

                        if !connection.isTerminal() {
                            pQueue.addNode(point: connection.id, priority: tempDistanceToConnection)
                        }
                    }
                }
            }

            let destinations = endPoints.filter({mPrevious[$0] != nil})
            if destinations.isEmpty {
                return [MapPosition]()
            }

            var result = [MapPosition]()

            // just choose first valid one
            var tempPointPath = destinations[0]
            while tempPointPath != start {
                result.append(tempPointPath)
                tempPointPath = mPrevious[tempPointPath]!
            }

            return result.reversed()
        }

        private class PQueue {
            private var pQueue = [PQueueNode]()

            init() {}

            func addNode(point: MapPosition, priority: Int?) {
                pQueue = pQueue.filter({$0.point != point})
                pQueue.append(PQueueNode(point: point, priority: priority!))
            }

            func getNextNode() -> MapPosition {
                var result:PQueueNode? = nil

                for node in pQueue {
                    if result == nil {
                        result = node
                    } else if node.priority < result!.priority {
                        result = node
                    }
                }

                pQueue = pQueue.filter({$0.point != result!.point})

                return result!.point
            }

            func size() -> Int {
                return pQueue.count
            }


            private class PQueueNode {
                var point: MapPosition
                var priority: Int
                init(point: MapPosition, priority: Int) {
                    self.point = point
                    self.priority = priority
                }
            }
        }
    }
}
