import SpriteKit

class PathFinderManager {
    static var shared = PathFinderManager()

    private var dikstra = Dikstra(nodes: [MapPosition: Node]())
    private var mNodes = [MapPosition: Node]()
    private init() {}

    func initMap(tiles: [MapPosition: Tile]) {
        mNodes.removeAll()

        buildNodes(tiles: tiles)
        buildConnections()

        dikstra = Dikstra(nodes: mNodes)
    }

    private func addBlocking(entities: [Entity]) {
        for entity in entities {
            if let node = mNodes[entity.mPosition] {
                node.mTypes.append(.ENTITY)
            }
        }
    }

    private func removeBlocking() {
        for node in mNodes {
            node.value.mTypes = node.value.mTypes.filter({$0 != .ENTITY})
        }
    }

    func getPath(start: MapPosition, end: MapPosition, blockingEntities: [Entity]) -> [MapPosition] {
        var endPoints = [MapPosition]()
        endPoints.append(end)
        return getPath(start: start, endPoints: endPoints, blockingEntities: blockingEntities)
    }

    func getPath(start: MapPosition, endPoints: [MapPosition], blockingEntities: [Entity]) -> [MapPosition] {
        //TODO: If monster is completely blocked from reaching player due to a monster being in the way
        // it will 'move backwards' as it found a path the long way
        addBlocking(entities: blockingEntities)
        let path = dikstra.getPath(start: start, endPoints: endPoints)
        removeBlocking()

        return path
    }

    private func buildNodes(tiles: [MapPosition: Tile]) {
        for tile in tiles {
            if !tile.value.mBlockMovement {
                var nodeTypes = [NodeType]()
                nodeTypes.append(.TILE)

                let node = Node(id: tile.value.mPosition, nodeTypes: nodeTypes)
                mNodes[node.id] = node
            }
        }
    }

    private func buildConnections() {
        for node in mNodes {
            for connection in getConnections(node: node.value) {
                node.value.mConnections.append(mNodes[connection]!)
            }
        }
    }

    private func getConnections(node: Node) -> [MapPosition] {
        var result = [MapPosition]()

        var possiblePoints = [MapPosition]()
        possiblePoints.append(MapPosition(x: node.id.x, y: node.id.y + 1))//north
        possiblePoints.append(MapPosition(x: node.id.x, y: node.id.y - 1))//south
        possiblePoints.append(MapPosition(x: node.id.x + 1, y: node.id.y))//east
        possiblePoints.append(MapPosition(x: node.id.x - 1, y: node.id.y))//west

        for point in possiblePoints {
            if isItConnected(node: node, point: point) {
                result.append(point)
            }
        }
        return result
    }

    private func isItConnected(node: Node, point: MapPosition) -> Bool {
        if mNodes[point] != nil && canConnect(typeA: node.mTypes, typeB: mNodes[point]!.mTypes) {
            return true
        }
        return false
    }

    private func canConnect(typeA: [NodeType], typeB: [NodeType]) -> Bool{
        if !typeA.filter({$0 == .TILE}).isEmpty && !typeB.filter({$0 == .TILE}).isEmpty {
            return true
        }

        return false
    }

    enum NodeType {
        case TILE, ENTITY
    }

    class Node {
        var id: MapPosition
        var mTypes: [NodeType]
        var mConnections = [Node]()

        init(id: MapPosition, nodeTypes: [NodeType]) {
            self.id = id
            self.mTypes = nodeTypes
        }

        func isTerminal() -> Bool {
            return false
        }

        func isBlocked() -> Bool {
            if mTypes.filter({$0 == .ENTITY}).isEmpty {
                return false
            }
            return true
        }

        func getNonBlockedConnections() -> [Node] {
            return mConnections.filter({!$0.isBlocked()})
        }
    }
}


