import SpriteKit

class Tile {
    var mSpriteNode = SKSpriteNode(color: .magenta, size: TILE_SIZE)

    private var mpBlockMovement = false
    private var mpBlockSight = false
    private var mpVisible = false

    var mPosition: MapPosition {
        get { return MapPosition(position: self.mSpriteNode.position) }
        set { self.mSpriteNode.position = newValue.getCGPoint() }
    }

    var mVisible: Bool {
        get { return mpVisible }
        set { mpVisible = newValue; calculateTexture() }
    }

    var mExplored: Bool {
        get { return !self.mSpriteNode.isHidden }
        set { self.mSpriteNode.isHidden = !newValue}
    }

    var mBlockMovement: Bool {
        get { return self.mpBlockMovement }
        set { self.mpBlockMovement = newValue; calculateTexture() }
    }

    var mBlockSight: Bool {
        get { return self.mpBlockSight }
        set { self.mpBlockSight = newValue; calculateTexture() }
    }

    init (position: MapPosition, blockMovement: Bool, blockSight: Bool) {
        mSpriteNode.zPosition = -100
        self.mSpriteNode.anchorPoint = CGPoint(x: 0, y: 0)
        self.mPosition = position
        self.mVisible = false
        self.mExplored = false
        self.mBlockMovement = blockMovement
        self.mBlockSight = blockSight
    }

    private func calculateTexture() {
        if self.mVisible {
            if self.mBlockSight {
                mSpriteNode.color = TextureManager.shared.getColor(objectName: "light_wall", isDead: false)
            } else {
                mSpriteNode.color = TextureManager.shared.getColor(objectName: "light_ground", isDead: false)
            }
        } else {
            if self.mBlockSight {
                mSpriteNode.color = TextureManager.shared.getColor(objectName: "dark_wall", isDead: false)
            } else {
                mSpriteNode.color = TextureManager.shared.getColor(objectName: "dark_ground", isDead: false)
            }
        }
    }
}
