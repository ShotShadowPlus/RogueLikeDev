import SpriteKit

class CommandCalculateVisibleTiles {
    private let mGameMap: GameMap
    private let mPlayerPosition: MapPosition
    private let mVisionRadius: UInt32

    var mVisibleTiles = [Tile]()

    init(gameMap: GameMap, playerPosition: MapPosition, visionRadius: UInt32) {
        self.mGameMap = gameMap
        self.mVisionRadius = visionRadius
        self.mPlayerPosition = playerPosition
    }

    func process() {
        for tile in mGameMap.mTiles {
            if isVisible(tile: tile.value) {
                mVisibleTiles.append(tile.value)
            }
        }
    }

    private func isVisible(tile: Tile) -> Bool {
        if mPlayerPosition.distance(to: tile.mPosition) < CGFloat(mVisionRadius) {
            return true
        }
        return false
    }
}

class CommandCalculateVisibleEntities {
    private let mEntities: [Entity]
    private let mPlayerPosition: MapPosition
    private let mVisionRadius: UInt32

    var mVisibleEntities = [Entity]()

    init(entities: [Entity], playerPosition: MapPosition, visionRadius: UInt32) {
        self.mEntities = entities
        self.mVisionRadius = visionRadius
        self.mPlayerPosition = playerPosition
    }

    func process() {
        for entity in mEntities {
            if isVisible(entity: entity) {
                mVisibleEntities.append(entity)
            }
        }
    }

    private func isVisible(entity: Entity) -> Bool {
        if mPlayerPosition.distance(to: entity.mPosition) < CGFloat(mVisionRadius) {
            return true
        }
        return false
    }
}

