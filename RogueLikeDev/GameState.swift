enum GameState {
    case PLAYERS_TURN
    case ENEMIES_TURN
    case GAME_OVER_INIT
    case GAME_OVER
}
