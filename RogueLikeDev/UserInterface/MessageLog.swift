import SpriteKit

class Message{
    var mText: String
    var mColor: NSColor

    init(text: String, color: NSColor) {
        self.mText = text
        self.mColor = color
    }
}

class MessageLog: SKSpriteNode {
    var mMessages = [SKLabelNode]()
    var mMaxLogSize = 4
    private let FONT_SIZE = CGFloat(10)

    init(size: CGSize) {
        super.init(texture: nil, color: .darkGray, size: size)

        self.zPosition = 100
    }

    func addMessage(_ newMessage: Message) {
        for message in mMessages {
            message.position.y += FONT_SIZE + 4
        }

        let newMessageLabel = SKLabelNode(text: newMessage.mText)
        newMessageLabel.fontColor = newMessage.mColor
        newMessageLabel.verticalAlignmentMode = .center
        newMessageLabel.horizontalAlignmentMode = .left
        newMessageLabel.fontName = "ChalkDuster"
        newMessageLabel.fontSize = FONT_SIZE
        newMessageLabel.position = CGPoint(x: self.size.width/(-2) + 4,
                                      y: self.size.height/(-2) + FONT_SIZE/2)
        self.addChild(newMessageLabel)
        mMessages.append(newMessageLabel)

        if mMessages.count > mMaxLogSize {
            let oldestMessage = mMessages.removeFirst()
            oldestMessage.removeFromParent()
        }
    }

    func clearLogMessages() {
        for message in mMessages {
            message.removeFromParent()
        }
        mMessages.removeAll()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
