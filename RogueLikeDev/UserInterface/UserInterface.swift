import SpriteKit

class UserInterface: SKNode {
    // TODO: not sure why make it tile size yet but here we are
    let mPanelHeight = CGFloat(12 * TILE_SIZE.height)
    let mPanelY: CGFloat

    let mPanelSprite = SKSpriteNode(color: .orange, size: CGSize(width: 1, height: 1))

    let mHPBar: StatusBar
    let mMessageLog: MessageLog
    let mSelectedLog: MessageLog

    init (screenSize: CGSize) {
        mPanelY = screenSize.height/(-2) + mPanelHeight/2

        mPanelSprite.size = CGSize(width: screenSize.width, height: mPanelHeight)
        mPanelSprite.position = CGPoint(x: 0, y: mPanelY)

        mHPBar = StatusBar(color: .red, size: CGSize(width: 20*TILE_SIZE.width, height: 2*TILE_SIZE.height))
        mHPBar.position = CGPoint(x: mPanelSprite.size.width/(-2) + mHPBar.size.width/2 + 2,
                                  y: mPanelSprite.size.height/2 - mHPBar.size.height/2 - 2)

        mMessageLog = MessageLog(size: CGSize(width: mPanelSprite.size.width*(1) - 2, height: 8*TILE_SIZE.height))
        mMessageLog.position = CGPoint(x: mPanelSprite.size.width*(0),
                                       y: mPanelSprite.size.height/(-2) + mMessageLog.size.height/2 + 2)
        mMessageLog.mMaxLogSize = 4

        mSelectedLog = MessageLog(size: CGSize(width: mPanelSprite.size.width*(0.125) - 2, height: 2*TILE_SIZE.height))
        mSelectedLog.position = CGPoint(x: mPanelSprite.size.width/(2) - mSelectedLog.size.width/2 - 2,
                                        y: mPanelSprite.size.height/(2) - mSelectedLog.size.height/2 - 2)
        mSelectedLog.mMaxLogSize = 1

        super.init()

        self.zPosition = 100
        self.addChild(mPanelSprite)
        mPanelSprite.addChild(mHPBar)
        mPanelSprite.addChild(mMessageLog)
        mPanelSprite.addChild(mSelectedLog)
    }

    func update(player: Entity) {
        mHPBar.update(currentValue: CGFloat(player.mFighterComponent!.mHP),
                      maxValue: CGFloat(player.mFighterComponent!.mMaxHP))
    }

    override func contains(_ p: CGPoint) -> Bool {
        if mPanelSprite.contains(convert(p, to: mPanelSprite)) {
            return true
        }
        return false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
