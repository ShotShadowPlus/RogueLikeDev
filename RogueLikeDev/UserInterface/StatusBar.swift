import SpriteKit

class StatusBar: SKSpriteNode {
    let mPercentageBar = SKSpriteNode()

    init (color: NSColor, size: CGSize) {
        super.init(texture: nil, color: .darkGray, size: size)

        mPercentageBar.color = color
        mPercentageBar.size = size
        mPercentageBar.zPosition = 110
        mPercentageBar.anchorPoint.x = 0
        mPercentageBar.position.x = size.width/(-2)

        update(currentValue: 20, maxValue: 100)
        self.zPosition = 100
        self.addChild(mPercentageBar)
    }

    func update(currentValue: CGFloat, maxValue: CGFloat) {
        mPercentageBar.size.width = self.size.width * (currentValue/maxValue)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
