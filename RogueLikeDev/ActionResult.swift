import SpriteKit

enum ActionResultType {
    case ACTION_RESULT_TYPE_MESSAGE
    case ACTION_RESULT_TYPE_DEAD
}

class ActionResult {
    var mActionResultType: ActionResultType
    var mMessage = ""
    var mInitiator: Entity?
    var mTarget: Entity?

    init(actionResultType: ActionResultType, message: String) {
        self.mActionResultType = actionResultType
        self.mMessage = message
    }
}
