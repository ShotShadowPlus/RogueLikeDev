import SpriteKit

enum KeyStateType {
    case KEY_STATE_TYPE_NONE
    case KEY_STATE_TYPE_UP
    case KEY_STATE_TYPE_DOWN
}

enum InputActionType {
    case INPUT_ACTION_TYPE_UNKNOWN
    case INPUT_ACTION_TYPE_MOVELEFT
    case INPUT_ACTION_TYPE_MOVERIGHT
    case INPUT_ACTION_TYPE_MOVEUP
    case INPUT_ACTION_TYPE_MOVEDOWN
    case INPUT_ACTION_TYPE_FULLSCREEN
}

class InputHandler {
    private var mInputActionKeyStateMap = [InputActionType: KeyStateType]()

    var mGameScene: GameScene!
    var mGameMap: GameMap!
    var mUserInterface: UserInterface!

    init() {

    }

    // This should be raw input and converted to at the scene level
    func processMouseInputDown(position: CGPoint) {
    }

    // This should be raw input and not converted at all yet
    func processMouseInputUp(position: CGPoint) {
        if mUserInterface.contains(mGameScene.convert(position, to: mUserInterface)) {
            // TODO: see if we want to handle mouse clicks on UI
            return
        }

        let mapPos = MapPosition(position: position)
        mapPos.x = mapPos.x.rounded(.down); mapPos.y = mapPos.y.rounded(.down)

        let entities = EntityManager.shared.getEntities(at: mapPos)
        if let entity = entities.first {
            if entity.mVisible {
                let message = Message(text: "\(entity.mName)", color: .white)
                mGameScene.mUserInterface.mSelectedLog.addMessage(message)
            } else {
                mGameScene.mUserInterface.mSelectedLog.clearLogMessages()
            }
        } else {
            mGameScene.mUserInterface.mSelectedLog.clearLogMessages()
        }
    }

    func processKeyboardInputKeyDown(keyCode: UInt16) {
        switch keyCode {
        case 49: // spacebar
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_FULLSCREEN] = .KEY_STATE_TYPE_DOWN
        case 123: // left
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVELEFT] = .KEY_STATE_TYPE_DOWN
        case 124: // right
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVERIGHT] = .KEY_STATE_TYPE_DOWN
        case 126: // up
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEUP] = .KEY_STATE_TYPE_DOWN
        case 125: // down
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEDOWN] = .KEY_STATE_TYPE_DOWN
        default:
            print("[Desc=KeyDown not handled] [keyCode: \(keyCode)]")
        }
    }

    func processKeyboardInputKeyUp(keyCode: UInt16) {
        switch keyCode {
        case 49: // spacebar
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_FULLSCREEN] = .KEY_STATE_TYPE_UP
        case 123: // left
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVELEFT] = .KEY_STATE_TYPE_UP
        case 124: // right
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVERIGHT] = .KEY_STATE_TYPE_UP
        case 126: // up
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEUP] = .KEY_STATE_TYPE_UP
        case 125: // down
            mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEDOWN] = .KEY_STATE_TYPE_UP
        default:
            print("[Desc=KeyUp not handled] [keyCode: \(keyCode)]")
        }
    }

    func getKeyState(inputActionType: InputActionType) -> KeyStateType {
        if mInputActionKeyStateMap[inputActionType] != nil {
            return mInputActionKeyStateMap[inputActionType]!
        }
        return .KEY_STATE_TYPE_NONE
    }

    func reset() {
        for inputActionType in mInputActionKeyStateMap.filter({$0.value == .KEY_STATE_TYPE_UP}) {
            mInputActionKeyStateMap[inputActionType.key] = .KEY_STATE_TYPE_NONE
        }
    }

    private func createMoveAction(distance: CGFloat) -> Action? {
        let action = Action()
        action.actionType = .ACTION_TYPE_MOVE

        if mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVELEFT] == .KEY_STATE_TYPE_UP {
            action.vector.dx = distance * -1
        }
        if mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVERIGHT] == .KEY_STATE_TYPE_UP {
            action.vector.dx = distance
        }
        if mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEUP] == .KEY_STATE_TYPE_UP {
            action.vector.dy = distance
        }
        if mInputActionKeyStateMap[.INPUT_ACTION_TYPE_MOVEDOWN] == .KEY_STATE_TYPE_UP {
            action.vector.dy = distance * -1
        }

        //action.vector = action.vector.normalize().scale(distance)

        if action.vector.isZeroVector() {
            return nil
        }
        return action
    }

    private func createFullScreenAction() -> Action? {
        if getKeyState(inputActionType: .INPUT_ACTION_TYPE_FULLSCREEN) == .KEY_STATE_TYPE_UP {
            let action = Action()
            action.actionType = .ACTION_TYPE_FULLSCREEN
            return action
        }
        return nil
    }


    func createActions() -> [Action] {
        var actions = [Action]()

        // assuming width/height are the same
        if let moveAction = createMoveAction(distance: 1) {
            actions.append(moveAction)
        }

        if let fullScreenAction = createFullScreenAction() {
            actions.append(fullScreenAction)
        }

        return actions
    }
}
