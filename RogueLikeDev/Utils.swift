import SpriteKit

extension CGRect {
    func getCenter() -> MapPosition{
        return MapPosition(x: (self.minX + self.maxX)/2, y: (self.minY + self.maxY)/2)
    }
}

extension CGPoint: Hashable {
    public var hashValue: Int {
        return self.x.hashValue << MemoryLayout<CGFloat>.size ^ self.y.hashValue
    }

    func add(vector: CGVector) -> CGPoint {
        return CGPoint(x: self.x + vector.dx ,y: self.y + vector.dy)
    }

    func getVector(to end: CGPoint) -> CGVector {
        return CGVector(dx: end.x - self.x, dy: end.y - self.y)
    }

    func distance(to end: CGPoint) -> CGFloat {
        return self.getVector(to: end).getMagnitude()
    }
}

extension CGVector {
    func isZeroVector() -> Bool {
        if self.dx == 0 && self.dy == 0 {
            return true
        }

        return false
    }

    func getMagnitude() -> CGFloat {
        return sqrt(self.dx * self.dx + self.dy * self.dy)
    }

    func normalize() -> CGVector {
        var retval = CGVector(dx: 0, dy: 0)
        if isZeroVector() {
            return retval
        }

        let magnitude = getMagnitude()

        retval.dx = self.dx/magnitude
        retval.dy = self.dy/magnitude
        return retval
    }

    func scale(_ value: CGFloat) -> CGVector {
        return CGVector(dx: self.dx * value, dy: self.dy * value)
    }
}
