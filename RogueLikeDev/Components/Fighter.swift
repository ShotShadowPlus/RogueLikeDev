class FighterComponent: EntityComponent {
    var mMaxHP: Int
    var mHP: Int
    var mDefense: Int
    var mPower: Int

    override init() {
        self.mMaxHP = 0
        self.mHP = 0
        self.mDefense = 0
        self.mPower = 0

        super.init()
    }

    init(hp: Int, defense: Int, power: Int) {
        self.mMaxHP = hp
        self.mHP = hp
        self.mDefense = defense
        self.mPower = power

        super.init()
    }

    func takeDamage(attacker: Entity, amount: Int) -> [ActionResult] {
        var actionResults = [ActionResult]()

        self.mHP -= amount

        if self.mHP <= 0 {
            let attackActionResult = ActionResult(actionResultType: .ACTION_RESULT_TYPE_DEAD,
                                              message: "\(mEntity.mName.capitalized) takes \(amount) damage from \(attacker.mName.capitalized) and has died." )
            attackActionResult.mInitiator = attacker
            attackActionResult.mTarget = mEntity

            actionResults.append(attackActionResult)
        } else {
            actionResults.append(ActionResult(actionResultType: .ACTION_RESULT_TYPE_MESSAGE,
                                              message: "\(mEntity.mName.capitalized) takes \(amount) damage from \(attacker.mName.capitalized)." ))
        }

        return actionResults
    }

    func attack(target: Entity) -> [ActionResult] {
        var actionResults = [ActionResult]()

        guard target.mFighterComponent != nil else {
            print ("[Desc=Trying to attack a target without a fighterComponent]")
            return actionResults
        }

        let damage = mPower - target.mFighterComponent!.mDefense

        if damage > 0 {
            actionResults.append(contentsOf: target.mFighterComponent!.takeDamage(attacker: mEntity, amount: damage))
        } else {
            let actionResult = ActionResult(actionResultType: .ACTION_RESULT_TYPE_MESSAGE,
                                            message: "\(mEntity.mName.capitalized) attacks \(target.mName.capitalized), but does no damage.")

            actionResults.append(actionResult)
        }

        return actionResults
    }
}

class Fighter: FighterComponent {
    override init(hp: Int, defense: Int, power: Int) {
        super.init(hp: hp, defense: defense, power: power)
    }


}
