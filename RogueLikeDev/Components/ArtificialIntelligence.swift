class AIComponent: EntityComponent {
    override init() { super.init() }

    func takeTurn(gameMap: GameMap) -> [ActionResult] { return [ActionResult]() }
}

class BasicMonster: AIComponent {
    override init() { super.init() }

    override func takeTurn(gameMap: GameMap) -> [ActionResult] {
        var actionResults = [ActionResult]()

        if mEntity.mFighterComponent!.mHP <= 0 {
            return actionResults
        }

        let player = EntityManager.shared.getPlayer()

        if gameMap.canSeePlayer(entity: mEntity) {
            if player.mFighterComponent!.mHP > 0 {
                actionResults.append(contentsOf: mEntity.mFighterComponent!.attack(target: player))
            }
        } else {
            if mEntity.mPosition.distance(to: player.mPosition) < 8 {
                let entitiesThatBlock = EntityManager.shared.getNPCs().filter({$0 !== mEntity})
                let path = PathFinderManager.shared.getPath(start: mEntity.mPosition, end: player.mPosition, blockingEntities: entitiesThatBlock)

                if !path.isEmpty {
                    mEntity.move(to: path.first!, gameMap: gameMap)
                }
            }
        }

        return actionResults
    }
}
