import Cocoa
import SpriteKit
import GameplayKit

class ViewController: NSViewController {

    @IBOutlet var skView: SKView!

    var mIsInFullScreenMode = false

    let SCREEN_SIZE = CGSize(width: 80*TILE_SIZE.width, height: 45*TILE_SIZE.height)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let view = self.skView {
            // Load the SKScene from 'GameScene.sks'
            if let scene = GameScene(fileNamed: "GameScene") {
                scene.mViewController = self

                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFit
                scene.size = SCREEN_SIZE
                scene.anchorPoint = CGPoint(x: 0, y: 0)

                //view.setFrameSize(SCREEN_SIZE)

                // Present the scene
                view.presentScene(scene)
            }
            view.ignoresSiblingOrder = true
            
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }


    func toggleFullScreenMode() {
        let window = self.skView.window!
        if mIsInFullScreenMode { // Exit full Screen Mode
            mIsInFullScreenMode = false
            NSMenu.setMenuBarVisible(true)
        } else { // Enter full Screen Mode
            mIsInFullScreenMode = true
            NSMenu.setMenuBarVisible(false)
        }
        window.toggleFullScreen(nil)
    }
}

