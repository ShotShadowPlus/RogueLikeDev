import SpriteKit
import GameplayKit

let MAP_SIZE = CGSize(width: 80, height: 45)

let ROOM_MIN_SIZE = 6
let ROOM_MAX_SIZE = 10
let MAX_ROOM_COUNT = 30
let MAX_MONSTERS_PER_ROOM = UInt32(3)

class GameScene: SKScene {
    let mCamera = SKCameraNode()

    var mViewController: ViewController!
    let mInputHandler = InputHandler()
    var mGameState = GameState.PLAYERS_TURN

    var mInited = false
    var entities = [Entity]()

    var mGameMap = GameMap(size: MAP_SIZE)
    var mUserInterface: UserInterface!

    override func didMove(to view: SKView) {
        mInputHandler.reset()

        if !mInited {
            camera = mCamera

            mGameMap.makeMap(maxRoomCount: UInt32(MAX_ROOM_COUNT), roomMinSize: UInt32(ROOM_MIN_SIZE), roomMaxSize: UInt32(ROOM_MAX_SIZE), mapSize: MAP_SIZE, maxMonstersPerRoom: MAX_MONSTERS_PER_ROOM)

            PathFinderManager.shared.initMap(tiles: mGameMap.mTiles)

            let player = EntityManager.shared.getPlayer()
            player.mPosition = MapPosition(x: Int(mGameMap.mRooms.first!.getCenter().x),
                                           y: Int(mGameMap.mRooms.first!.getCenter().y))

            for tile in mGameMap.mTiles {
                self.addChild(tile.value.mSpriteNode)
            }

            mGameMap.calculatePlayerVision(player: player)

            mUserInterface = UserInterface(screenSize: self.size)
            self.addChild(mUserInterface)

            mInputHandler.mGameMap = mGameMap
            mInputHandler.mUserInterface = mUserInterface
            mInputHandler.mGameScene = self
        }
    }
    
    func touchDown(atPoint pos : CGPoint) { }
    
    func touchMoved(toPoint pos : CGPoint) { }
    
    func touchUp(atPoint pos : CGPoint) {
        mInputHandler.processMouseInputUp(position: pos)
    }
    
    override func mouseDown(with event: NSEvent) {
        self.touchDown(atPoint: event.location(in: self))
    }
    
    override func mouseDragged(with event: NSEvent) {
        self.touchMoved(toPoint: event.location(in: self))
    }
    
    override func mouseUp(with event: NSEvent) {
        self.touchUp(atPoint: event.location(in: self))
    }

    override func keyDown(with event: NSEvent) {
        mInputHandler.processKeyboardInputKeyDown(keyCode: event.keyCode)
    }

    override func keyUp(with event: NSEvent) {
        mInputHandler.processKeyboardInputKeyUp(keyCode: event.keyCode)
    }

    func processPlayersTurn() {
        for actionResult in processActions() {
            switch actionResult.mActionResultType{
            case .ACTION_RESULT_TYPE_DEAD:
                killNPC(actionResult.mTarget!)
                let message = Message(text: actionResult.mMessage, color: .orange)
                mUserInterface.mMessageLog.addMessage(message)
            case .ACTION_RESULT_TYPE_MESSAGE:
                let message = Message(text: actionResult.mMessage, color: .white)
                mUserInterface.mMessageLog.addMessage(message)
            }
        }
    }

    func processEnemiesTurn() {
        for npc in EntityManager.shared.getNPCs() {
            for actionResult in npc.takeTurn(gameMap: mGameMap) {
                switch actionResult.mActionResultType{
                case .ACTION_RESULT_TYPE_DEAD:
                    if actionResult.mTarget === EntityManager.shared.getPlayer() {
                        killPlayer(actionResult.mTarget!)
                        mGameState = .GAME_OVER_INIT

                        let message = Message(text: actionResult.mMessage, color: .red)
                        mUserInterface.mMessageLog.addMessage(message)
                        return
                    }
                case .ACTION_RESULT_TYPE_MESSAGE:
                    let message = Message(text: actionResult.mMessage, color: .white)
                    mUserInterface.mMessageLog.addMessage(message)
                }
            }
        }
        mGameState = .PLAYERS_TURN
    }
    
    func processActions () -> [ActionResult] {
        var actionResults = [ActionResult]()
        let player = EntityManager.shared.getPlayer()

        for action in mInputHandler.createActions() {
            switch action.actionType {
            case .ACTION_TYPE_MOVE:
                if !mGameMap.isBlocked(position: player.mPosition.add(vector: action.vector) ) {
                    let blockingEntities = EntityManager.shared.getEntities(at: player.mPosition.add(vector: action.vector)).filter({$0.mBlocks == true})

                    if blockingEntities.isEmpty {
                        player.move(action.vector)
                        mGameMap.calculatePlayerVision(player: player)
                    } else {
                        actionResults.append(contentsOf: player.mFighterComponent!.attack(target: blockingEntities.first!))
                    }

                    mGameState = .ENEMIES_TURN
                }
            case .ACTION_TYPE_FULLSCREEN:
                mViewController.toggleFullScreenMode()
            default:
                break
            }
        }
        mInputHandler.reset()

        return actionResults
    }

    func processGameOverInit() {
        print("You have Died.")
        mGameState = .GAME_OVER
    }

    func processGameOver() {
        // TODO: listen for input to restart the game
    }

    func processGameEntities() {
        let player = EntityManager.shared.getPlayer()
        let commandCalculateVision = CommandCalculateVisibleEntities(entities: EntityManager.shared.getNPCs(), playerPosition: player.mPosition, visionRadius: player.mVisionRadius)
        commandCalculateVision.process()

        for npc in EntityManager.shared.getNPCs() {
            npc.mVisible = false
        }

        for npc in commandCalculateVision.mVisibleEntities {
            npc.mVisible = true
        }
    }

    func processEngine() {
        for entity in EntityManager.shared.getNPCsToBeAttached() {
            self.addChild(entity.mSpriteNode)
        }

        EntityManager.shared.clearNPCsToBeAttached()

        mCamera.position = EntityManager.shared.getPlayer().mPosition.getCGPoint()

        mUserInterface.position = mCamera.position
        mUserInterface.update(player: EntityManager.shared.getPlayer())
    }

    override func update(_ currentTime: TimeInterval) {
        switch mGameState {
        case .PLAYERS_TURN:
            processPlayersTurn()
        case .ENEMIES_TURN:
            processEnemiesTurn()
        case .GAME_OVER_INIT:
            processGameOverInit()
        case .GAME_OVER:
            processGameOver()
        }
        processGameEntities()
        processEngine()
    }
}
