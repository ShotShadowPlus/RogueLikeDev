import SpriteKit

enum ActionType {
    case ACTION_TYPE_UNKNOWN
    case ACTION_TYPE_FULLSCREEN
    case ACTION_TYPE_MOVE
}

class Action {
    var actionType = ActionType.ACTION_TYPE_UNKNOWN
    var vector = CGVector()

    init() {}    
}
