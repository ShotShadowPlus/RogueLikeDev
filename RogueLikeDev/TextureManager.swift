import SpriteKit

class TextureManager {
    static var shared = TextureManager()
    private var mColors = [String: NSColor]()

    private init() {
        mColors["player"] = NSColor(red: 255/255, green: 0, blue: 0, alpha: 1)
        mColors["player_dead"] = NSColor(red: 100/255, green: 0, blue: 0, alpha: 1)
        mColors["npc"] = NSColor(red: 255/255, green: 255/255, blue: 0, alpha: 1)
        mColors["orc"] = NSColor(red: 0/255, green: 255/255, blue: 0, alpha: 1)
        mColors["orc_dead"] = NSColor(red: 0/255, green: 100/255, blue: 0, alpha: 1)
        mColors["troll"] = NSColor(red: 0/255, green: 255/255, blue: 255/0, alpha: 1)
        mColors["troll_dead"] = NSColor(red: 0/255, green: 100/255, blue: 100/0, alpha: 1)

        mColors["dark_wall"] = NSColor(red: 0, green: 0, blue: 100/255, alpha: 1)
        mColors["dark_ground"] = NSColor(red: 50/255, green: 50/255, blue: 150/255, alpha: 1)
        mColors["light_wall"] = NSColor(red: 130/255, green: 110/255, blue: 50/255, alpha: 1)
        mColors["light_ground"] = NSColor(red: 200/255, green: 180/255, blue: 50/255, alpha: 1)
    }

    func getColor(objectName: String, isDead: Bool) -> NSColor {
        var tempObjectName = objectName
        if isDead {
            tempObjectName = objectName + "_dead"
        }

        if let objectColor = mColors[tempObjectName] {
            return objectColor
        }
        return .magenta
    }

}
