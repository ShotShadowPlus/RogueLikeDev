import SpriteKit
import Foundation

// assuming width/height are the same
let TILE_SIZE = CGSize(width: 8, height: 8)

class MapPosition: Hashable {
    var x: CGFloat
    var y: CGFloat

    init(x: CGFloat, y: CGFloat) {
        self.x = x
        self.y = y
    }

    init(x: Int, y: Int) {
        self.x = CGFloat(x)
        self.y = CGFloat(y)
    }

    convenience init() {
        self.init(x: 0, y: 0)
    }

    convenience init(position: CGPoint) {
        // for truncating remainders
        self.init(x: position.x/TILE_SIZE.width,
                  y: position.y/TILE_SIZE.height)
    }

    func getCGPoint() -> CGPoint {
        return CGPoint(x: TILE_SIZE.width * self.x,
                       y: TILE_SIZE.height * self.y)
    }

    func add(vector: CGVector) -> MapPosition {
        return MapPosition(x: self.x + vector.dx ,y: self.y + vector.dy)
    }

    func getVector(to end: MapPosition) -> CGVector {
        return CGVector(dx: end.x - self.x, dy: end.y - self.y)
    }

    func distance(to end: MapPosition) -> CGFloat {
        return self.getVector(to: end).getMagnitude()
    }

    static func == (lhs: MapPosition, rhs: MapPosition) -> Bool {
        return lhs.x == rhs.x && lhs.y == rhs.y
    }

    var hashValue: Int {
        return self.x.hashValue << MemoryLayout<CGFloat>.size ^ self.y.hashValue
    }
}
